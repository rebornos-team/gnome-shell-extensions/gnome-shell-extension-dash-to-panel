# gnome-shell-extension-dash-to-panel

Extension for GNOME shell to combine the dash and main panel

https://github.com/jderose9/dash-to-panel

How to clone this repo:

```
git clone https://gitlab.com/reborn-os-team/gnome-shell-extensions/gnome-shell-extension-dash-to-panel.git
```

<br><br>
# Original PKGBUILD:

```
# Maintainer: Benoit Brummer < trougnouf at gmail dot com >
# Contributor: Carl George < arch at cgtx dot us >

pkgname=gnome-shell-extension-dash-to-panel
_name=dash-to-panel
pkgver=40
pkgrel=1
pkgdesc='Extension for GNOME shell to combine the dash and main panel'
arch=(any)
url="https://github.com/home-sweet-gnome/dash-to-panel"
license=(GPL2)
makedepends=(git gnome-common intltool make)
source=("${url}/archive/v${pkgver}.tar.gz")
sha256sums=('38729041ee6db81284f555a0b01f66583d88252c86ebb3989f390dedbb6f644f')

build() {
    cd "${srcdir}/${_name}-${pkgver}"
    make VERSION="$pkgver" _build
}

package() {
    depends=('gnome-shell')
    cd "${srcdir}/${_name}-${pkgver}"
    make DESTDIR="$pkgdir" install
}
```

<br><br>
# PKGBUILD GNOME 40 OTHER REPO:

```
# Maintainer: Benoit Brummer < trougnouf at gmail dot com >
# Contributor: Carl George < arch at cgtx dot us >
# Modified by: Rafael from RebornOS. Momentarily using another git repository

pkgname=gnome-shell-extension-dash-to-panel
_name=dash-to-panel
pkgver=40
pkgrel=2.5
pkgdesc='Extension for GNOME shell to combine the dash and main panel'
arch=(any)
url="https://github.com/philippun1/dash-to-panel"
license=(GPL2)
makedepends=(git gnome-common intltool make)
source=("git+${url}.git#branch=update-to-gnome40")
sha256sums=('SKIP')

build() {
    cd "${srcdir}/${_name}"
    make VERSION="$pkgver" _build
}

package() {
    depends=('gnome-shell')
    cd "${srcdir}/${_name}"
    make DESTDIR="$pkgdir" install
}
```

<br><br>
# PKGBUILD WITH COMMIT:

```
# Maintainer: Benoit Brummer < trougnouf at gmail dot com >
# Contributor: Carl George < arch at cgtx dot us >
# Modified by: Rafael from RebornOS. Momentarily using another git repository

pkgname=gnome-shell-extension-dash-to-panel
_name=dash-to-panel
commitn=ab730b9270f1130154baeeb8fb1116ca7fba9741
pkgver=43
pkgrel=1.1
pkgdesc='Extension for GNOME shell to combine the dash and main panel'
arch=(any)
url="https://github.com/home-sweet-gnome/dash-to-panel"
license=(GPL2)
makedepends=(git gnome-common intltool make)
source=("git+${url}#commit=${commitn}")
sha512sums=('SKIP')

build() {
    cd ${srcdir}/${_name}
    make VERSION="$pkgver" _build
}

package() {
    depends=('gnome-shell')
    cd ${srcdir}/${_name}
    make DESTDIR="$pkgdir" install
}
```

